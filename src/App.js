import React, { Component } from 'react';
import './App.css';

import Add from './Add'
import CountDisplay from './CountDisplay'
import Subtract from './Subtract'

class App extends Component {
  render() {
    return (
      <div className="App">
        <CountDisplay />
        <Add />
        <Add />
        <Subtract />
        <Add />
        <CountDisplay />
        <Subtract />
        <Add />
      </div>
    );
  }
}

export default App;
