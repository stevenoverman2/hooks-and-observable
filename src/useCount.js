import React from 'react'
import CountService from './CountService';

const useCount = () => {
  const [count, setCount] = React.useState()

  React.useEffect(() => {
    CountService.countObservable.subscribe(newCount => setCount(newCount));
    return () => CountService.countObservable.unsubscribe();
  }, [])

  return count;
}

export default useCount;
