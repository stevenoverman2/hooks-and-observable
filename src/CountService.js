import { BehaviorSubject } from 'rxjs'

let count = 0
const countObservable = new BehaviorSubject([count])

const addOne = () => {
  count += 1
  countObservable.next(count)
}

const subtractOne = () => {
  count = Math.max(count - 1, 0)
  countObservable.next(count)
}

export default {
  addOne,
  countObservable,
  subtractOne,
}
