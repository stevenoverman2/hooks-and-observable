import React from 'react'
import useCount from './useCount'

const CountDisplay = () => {
  const count = useCount();

  return (
    <div>Count: {count}</div>
  )
}

export default CountDisplay
