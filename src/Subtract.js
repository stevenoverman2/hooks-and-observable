import React from 'react'
import CountService from './CountService'

const Subtract = () => {
  return (
    <button onClick={() => CountService.subtractOne()}>Subtract</button>
  )
}

export default Subtract
