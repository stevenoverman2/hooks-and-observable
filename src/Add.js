import React from 'react'
import CountService from './CountService'

const Add = () => {
  return (
    <button onClick={() => CountService.addOne()}>Add</button>
  )
}

export default Add
